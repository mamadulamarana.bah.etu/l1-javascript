// cibles.js
const TARGET_WIDTH = 40;
const TARGET_HEIGHT = 40;
let nbre_restants;
const start = document.getElementById("start");


const field = document.getElementById("field");
const Terrain_Height = field.clientHeight;
const Terrain_Width = field.clientWidth;

function alea(n) {
    return Math.floor(Math.random() * n);
}

function createOneTarget() {
    const element = document.createElement("div");
    element.classList.add("target");
    const top_val = alea(Terrain_Height - TARGET_HEIGHT);
    const left_val = alea(Terrain_Width - TARGET_WIDTH);
    element.style.top = top_val + "px";
    element.style.left = left_val + "px";
    element.addEventListener("click", destroyTarget);
    field.appendChild(element);
}

function createAllTargets() {
    const nbre_cibles = document.getElementById("nbtargets").value;
    console.log(nbre_cibles)
    const cibles_restantes = document.getElementById("remaining")
    for (let i = 0; i < nbre_cibles; i++) {
        createOneTarget();
        nbre_restants = nbre_cibles;
        cibles_restantes.textContent = nbre_restants;
    }
    start.removeEventListener("click", createAllTargets);
    start.disabled = true;
}

function destroyTarget() {
    this.parentElement.removeChild(this);
    nbre_restants -= 1;
    const cibles_restantes = document.getElementById("remaining")
    cibles_restantes.textContent = nbre_restants;
    if (nbre_restants === 0) {
        start.addEventListener("click", createAllTargets)
        start.disabled = false;
    }
}





function SetupListeners() {
    const start = document.getElementById("start");
    start.addEventListener("click", createAllTargets);

}
SetupListeners();