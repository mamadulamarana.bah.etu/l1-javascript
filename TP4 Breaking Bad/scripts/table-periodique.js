// Q2
const focus1 = function (event) {
    element = document.getElementById("focus");
    const contenu = event.target.innerHTML;
    element.innerHTML = contenu;
}

// Q3
//1 => div.ligne div.elementChimique

//2
const focusInitial = function () {
    const elements = document.querySelector("div.ligne div.elementChimique");
    const elmnt = document.getElementById("focus");
    elmnt.innerHTML = elements.innerHTML;
}

// Q4
//1 => display = "none" pour masquer et display = "" pour reafficher l'element

//2
const masqueOrAffiche = function (event) {
    const element = document.getElementById("focus")
    if (event.target.checked === true) {
        element.style.display = "";
    }
    else {
        element.style.display = "none";
    }
}

// Q5
//1 => div.ligne div.masseatomique
//2
const displayMasse = function (event) {
    const elements = document.querySelectorAll("div.ligne div.masseatomique");
    for (let i = 0; i < elements.length; i++)
        if (event.target.checked == true) {
            elements[i].style.display = "";
        }
        else {
            elements[i].style.display = "none";
        }
}

const displayCouche = function (event) {
    const elements = document.querySelectorAll("div.ligne div.coucheselectrons");
    for (let i = 0; i < elements.length; i++)
        if (event.target.checked == true) {
            elements[i].style.display = "";
        }
        else {
            elements[i].style.display = "none";
        }
}


const displayElectro = function (event) {
    const elements = document.querySelectorAll("div.ligne div.electronegativite");
    for (let i = 0; i < elements.length; i++) {
        if (event.target.checked == true) {
            elements[i].style.display = "";
        }
        else {
            elements[i].style.display = "none";
        }
    }
}

/**function prop(event) {
    let elements;
    const array1 = ["div.ligne div.masseatomique", "div.ligne div.coucheselectrons", "div.ligne div.electronegativite"];
    for (let j = 0; j < array1.length; j++) {
        elements = document.querySelectorAll(array1[j])
        for (let i = 0; i < element.length; i++) {
            if (event.target.checked == true) {
                elements[i].style.display = "";
            }
            else {
                elements[i].style.display = "none";
            }
        }
    }
}**/

// Q7
function focus3(event) {
    element = document.getElementById("focus");
    const contenu = event.target.innerHTML;
    element.innerHTML = contenu;
    for (const elmnt of element.querySelectorAll("*")) {
        elmnt.style.display = "";
    }
}

// Q8
//1 #checkOptions input  et div#checkOptions > input

//2
function selectTous() {
    for (const element of document.querySelectorAll("div#checkOptions > input")) {
        element.click();
        element.checked = true;
    }
}

function selectAucun() {
    for (const element of document.querySelectorAll("div#checkOptions input")) {
        element.click();
        element.checked = false;
    }
}



// Execution des Events
const SetupListeners = function () {
    for (const elmnt of document.getElementsByClassName("elementChimique")) {
        elmnt.addEventListener("mouseover", focus3);
    }
    focusInitial();

    const focus = document.getElementById("checkFocus");
    focus.addEventListener("change", masqueOrAffiche);

    const masse = document.getElementById("checkMasse");
    masse.addEventListener("change", displayMasse);

    const couches = document.getElementById("checkCouches");
    couches.addEventListener("change", displayCouche);

    const electro = document.getElementById("checkElectro");
    electro.addEventListener("change", displayElectro);

    const tous = document.getElementById("tous");
    tous.addEventListener("click", selectTous);

    const aucun = document.getElementById("aucun");
    aucun.addEventListener("click", selectAucun);
}
SetupListeners()



