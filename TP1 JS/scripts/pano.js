// mise en place des événements
const setupListeners = function(){
    var pano = document.getElementById("panoramique");
    pano.addEventListener("mouseover", changePano);
    pano.addEventListener("mouseout", changePano2);
}
// on prépare l'exécution de setupListeners après la chargement du document
window.addEventListener("load", setupListeners);


var changeWidth = function (img, n) {
    var x = document.getElementById(img);
    x.style.width = n
}
// changeWidth("panoramique", "400px")

var changePano = function () {
    var pano = document.getElementById("panoramique");
    pano.src = "images/panoramique2.jpg"
}

// changePano2
var changePano2 = function () {
    var pano = document.getElementById("panoramique");
    pano.src = "images/panoramique.jpg"
}