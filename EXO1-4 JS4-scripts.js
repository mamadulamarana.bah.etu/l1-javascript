// Q2
const tousVerts1 = function (balise) {
    // for (let i = 0; i < lesElements.length; i++){
    //      lesElements[i].style.color = "green";
    // }
    for (const elmnt of document.getElementsByTagName(balise)) {
        elmnt.style.color = "green";
    }
}
//tousVerts1("em")

// Q4
const tousVerts2 = function (noeud1, noeud2) {
    const el1 = document.getElementById(noeud2)
    for (const elmnt of el1.getElementsByTagName(noeud1)) {
        elmnt.style.color = "green";
    }
}
//tousVerts2("em", "gr34")

// Q5
const tousVerts3 = function (noeud1, noeud2) {
    // noeud2 = noeud2 || document;
    let el1 = document;
    if (noeud2) {
        el1 = document.getElementById(noeud2);
    }
    for (const elmnt of el1.getElementsByTagName(noeud1)) {
        elmnt.style.color = "green";
    }
}
//tousVerts3("em", "gr34")

// EXO 2
// Q1
let imageRestaure;

const imageSurvol = function (event) {
    imageRestaure = event.target.src;
    event.target.src = "images/intrus.jpg";
    //peut etre remplacer par this =>> event.target.src = "images/intrus.jpg";
}

const imageRest = function (event) {
    console.log(event.target)
    event.target.src = imageRestaure;
}

// EXO 3
// Q1
/** for (const elmnt of document.getElementsByClassName("droite")){
    elmnt.style.padding = "20px"
  } **/

// Q2
/** element = document.getElementById("ajoncs");
for(const elmnt of element.getElementsByClassName("par")){
    elmnt.style.border = "solid 1px blue"
} **/

// Q3
const cache = function (classe) {
    for (const element of document.getElementsByClassName(classe)){
        element.style.display = "none";
    }
}
// cache("par")

// Q4
 const devoile = function(classe){
     for(const element of document.getElementsByClassName(classe)){
         element.style.display = "";
     }
 }
// setTimeout(() =>devoile("par"), 2000)
// devoile("par")





const SetupListeners = function () {
    for (const image of document.getElementsByTagName("img")) {
        image.addEventListener("mouseover", imageSurvol);
        image.addEventListener("mouseout", imageRest)
    }
}
// SetupListeners()

// EXO 4
// Q1
const elements = document.querySelectorAll("div#ajoncs div.par")
// console.log(elements)

// Q2
