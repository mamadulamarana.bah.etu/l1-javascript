// BAH MAMADU LAMARANA

// ===  variables globales === 

// constantes
const MAX_QTY = 9;

//  tableau des produits à acheter
const cart = [];

// total actuel des produits dans le panier
let total = 0;


// === initialisation au chargement de la page ===

/**
* Création du Magasin, mise à jour du total initial
* Mise en place du gestionnaire d'événements sur filter
* creation du boutton stockage du panier.
* remide en place du panier au chargement de la page avec getBasket.
*/
const init = function () {
	createShop();
	updateTotal();

	buttonStorage();
	getBasket();

	const filter = document.getElementById("filter");
	filter.addEventListener("keyup", filterDisplaidProducts);
}
window.addEventListener("load", init);


// ==================== fonctions utiles ======================= 

/**
* Crée et ajoute tous les éléments div.produit à l'élément div#boutique
* selon les objets présents dans la variable 'catalog'
*/
const createShop = function () {
	const shop = document.getElementById("boutique");
	for (let i = 0; i < catalog.length; i++) {
		shop.appendChild(createProduct(catalog[i], i));
	}
}

/**
* Crée un élément div.produit qui posséde un id de la forme "i-produit" où l'indice i 
* correspond au paramètre index
* @param {Object} product - le produit pour lequel l'élément est créé
* @param {number} index - l'indice (nombre entier) du produit dans le catalogue (utilisé pour l'id)
* @return {Element} une div.produit
*/
const createProduct = function (product, index) {
	// créer la div correspondant au produit
	const divProd = document.createElement("div");
	divProd.className = "produit";
	// fixe la valeur de l'id pour cette div
	divProd.id = index + "-product";
	// crée l'élément h4 dans cette div
	divProd.appendChild(createBlock("h4", product.name));

	// Ajoute une figure à la div.produit... 
	divProd.appendChild(createFigureBlock(product));

	// crée la div.description et l'ajoute à la div.produit
	divProd.appendChild(createBlock("div", product.description, "description"));
	// crée la div.prix et l'ajoute à la div.produit
	divProd.appendChild(createBlock("div", product.price, "prix"));
	// crée la div.controle et l'ajoute à la div.produit
	divProd.appendChild(createOrderControlBlock(index));
	return divProd;
}


/** Crée un nouvel élément avec son contenu et éventuellement une classe
 * @param {string} tag - le type de l'élément créé (example : "p")
 * @param {string} content - le contenu html de l'élément a créé  (example : "bla bla")
 * @param {string} [cssClass] - (optionnel) la valeur de l'attribut 'classe' de l'élément créé
 * @return {Element} élément créé
 */
const createBlock = function (tag, content, cssClass) {
	const element = document.createElement(tag);
	if (cssClass != undefined) {
		element.className = cssClass;
	}
	element.innerHTML = content;
	return element;
}

/** Met à jour le montant total du panier en utilisant la variable globale total
 */
const updateTotal = function () {
	const montant = document.getElementById("montant");
	montant.textContent = total;
}

// ======================= fonctions à compléter =======================


/**
* Crée un élément div.controle pour un objet produit
* @param {number} index - indice du produit considéré
* @return {Element}
* TODO : AJOUT des gestionnaires d'événements
*/
const createOrderControlBlock = function (index) {
	const control = document.createElement("div");
	control.className = "controle";

	// crée l'élément input permettant de saisir la quantité
	const input = document.createElement("input");
	input.id = index + "-qte";
	input.type = "number";
	input.step = "1";
	input.value = "0";
	input.min = "0";
	input.max = MAX_QTY.toString();

	// TODO :  Q5 mise en place du gestionnaire d'événément pour input permettant de contrôler les valeurs saisies
	control.appendChild(input);
	input.addEventListener("change", verifQuantity);

	// Crée le bouton de commande
	const button = document.createElement("button");
	button.className = 'commander';
	button.id = index + "-order";
	control.appendChild(button);

	return control;
}


/** 
* Crée un élément figure (img) correspondant pour un produit.
* @param {Object} product - le produit pour lequel la figure est créée
* @return {Element} figure crée
*/
const createFigureBlock = function (product) {

	const figure = document.createElement("figure"); // creation d'un element figure

	// creation d'un element image et son contenu (src, alt).
	const image = document.createElement("img");
	for (let i = 0; i < catalog.length; i++) {
		if (catalog[i] === product) {
			image.src = catalog[i].image;
			image.alt = catalog[i].description;
		}
	}

	figure.appendChild(image); // ajout de l'element image dans l'element figure.
	return figure;
}


/** 
* @todo gestion de la mise en panier du produit commandé et mise à jour de la quantité du produit dans la boutique.
*/
const orderProduct = function () {
	const idx = parseInt(this.id); // index du produit
	const qty = parseInt(document.getElementById(idx + "-qte").value); // quantité du produit

	if (qty > 0) {
		addProductToCart(idx, qty); // ajoute un produit au panier
		// gestion de la remise à zéro de la quantité après la commande 
		// et tous les comportements du bouton représentant le chariot.
		const commander = document.getElementById(idx + "-qte");
		commander.value = 0;
		this.style.opacity = 0.25;
	}
}


// ======================= fonctions à coder =======================



/**
* @todo verification de la validité de la quantité saisie et gestion du boutton panier (commander).
*/
const verifQuantity = function () {

	const boutton = document.getElementById(this.id.slice(0, -4) + "-order");

	// verification de la quantité saisie et gestion de l'opacity du boutton commander.
	if (parseInt(this.value) > 9 || parseInt(this.value) < 0) {
		this.value = 0;
	}

	else if (parseInt(this.value) > 0) {
		boutton.style.opacity = 1;
		boutton.addEventListener("click", orderProduct); // mise en place de l'evenement pour creer une commande.
	}

	else {
		boutton.style.opacity = 0.25;
	}
}



let boolqty = false; // variable bool témoin pour verifier la modification d'une quantité dans le panier.
/**
* @todo ajout d'un produit dans le panier et gestion de la mise à jour du total du panier.
* @param {number} index indice du produit
* @param {number} qty quantité du produit
*/
const addProductToCart = function (index, qty) {

	const buttonStorage1 = document.getElementById("storage"); // boutton stockage du panier.
	buttonStorage1.disabled = false; // desactivation du boutton stocker panier.


	const achats = document.getElementsByClassName("achats");

	// creation d'un div pour le produit commandé.
	const divAchat = createDivAchat(index, qty);

	// ajout du produit commandé dans le panier et dans le chariot(variable globale => cart), mise à jour du total.
	if (cart.indexOf(catalog[index]) === -1) { // verification de la presence du produit dans le panier.

		cart.push(catalog[index]); // ajout du produit dans le cart.

		const indCart = cart.indexOf(catalog[index]);
		cart[indCart].id = index + "-achat";
		cart[indCart].qty = qty;

		achats[0].appendChild(divAchat); // ajout dans le panier.

		montant(index, qty); // mise à jour de la variable total.
	}

	// modification de la quantité du produit déjà present dans le panier et gestion du total.
	else {
		boolqty = true; // variable booléenne pour verifier la modification d'une quantité dans le panier.

		const produit = document.getElementById(index + "-achat"); // produit à ajouter dans le panier.

		const modifQty = produit.getElementsByClassName("quantite"); // quantité du produit.

		const oldQty = parseInt(modifQty[0].textContent); // recuperation  de l'ancienne qty dans le panier avant modification.

		modifQty[0].textContent = qty; // modfication de la quantité dans le panier après un ajout de produit qui est déjà dans le panier.

		cart[cart.indexOf(catalog[index])].qty = qty; // modification de qty dans cart (utile pour stocker le panier)

		const oldTotal = total; // recuperation du  total avant modification.

		// mise à jour du montant.
		montant(index, qty, oldQty, oldTotal);

	}
	updateTotal(); // mise à jour du montant dans le panier.

	// mise en place de l'évenement pour retirer un produit dans le panier.
	const suppression = document.getElementById(index + "-remove");
	suppression.addEventListener("click", supprimeProduct);
}


/**
 * @todo creation d'un divAchat pour le produit commandé
 * @param {number} index indice du produit
 * @param {number} qty quantité du produit
 * @returns {Element} element crée divAchat
 */
function createDivAchat(index, qty) {
	// creation d'un element div de classe "achat" et son Id correspondant.
	const divAchat = document.createElement("div");
	divAchat.classList.add("achat");
	divAchat.id = index + "-achat";

	// creation de l'image du produit commandé.
	const figure = createFigureBlock(catalog[index]);
	divAchat.appendChild(figure);

	// creation du contenu de la divAchat crée (name, quantité, prix)
	divAchat.appendChild(createBlock("h4", catalog[index].name));
	divAchat.appendChild(createBlock("div", qty.toString(), "quantite"));
	divAchat.appendChild(createBlock("div", (catalog[index].price).toString(), "prix"));

	// creation de la div control (classe, id, button..) pour le produit commandé et l'ajouter à la divAchat.
	const divControle = document.createElement("div");
	divControle.classList.add("controle");
	const buttonRetirer = document.createElement("button");
	buttonRetirer.classList.add("retirer");
	buttonRetirer.id = index + "-remove";

	divControle.appendChild(buttonRetirer);
	divAchat.appendChild(divControle);

	return divAchat;
}


/**
 * @todo mise à jour de la variable total.
 * @param {number} index indice du produit
 * @param {number} qty quantité du produit
 * @param {number} oldQty quantité du produit dans le panier avant son ajout
 * @param {number} oldTotal total du panier avant modification
 */
function montant(index, qty, oldQty, oldTotal) {
	// mise à jour de la variable total selon ajout ou suppression.
	if (boolqty) {
		total = qty * catalog[index].price;
		total += oldTotal - oldQty * catalog[index].price;
		boolqty = false;
	}
	else {
		total += qty * catalog[index].price;
	}
}



/**
 * @todo getion de la suppression d'un produit dans le panier.
 */
function supprimeProduct() {

	const deleteProduct = this.parentNode.parentNode;
	deleteProduct.parentNode.removeChild(deleteProduct);

	const index = parseInt(deleteProduct.id); // indice du produit.

	cart.splice(cart.indexOf(catalog[index]), 1); //retrait du produit dans cart (varible global => cart).

	// mise à jour du total après suppression du produit dans le panier.
	const quantite = parseInt(deleteProduct.querySelector(".quantite").textContent);
	const prix = parseInt(deleteProduct.querySelector(".prix").textContent);
	total = total - quantite * prix;
	updateTotal(); // mise à jour du montant dans le panier.

	// desactivation du boutton pour stockage le panier.
	if (cart.length === 0) {
		const buttonStorage1 = document.getElementById("storage");
		buttonStorage1.disabled = true; // desactivation du boutton stockage panier
	};

}


/**
* @todo gestion de la filtration des produits avec la barre de recherche.
*/
const filterDisplaidProducts = function () {

	const filterText = this.value; // stocke la valeur du texte saisie dans la barre de recherche.

	const produits = document.getElementsByClassName("produit"); // NodeList des produits.

	// gestion de l'affichage des produits recherché avec la propriété display.
	for (const produit of produits) {
		const index = parseInt(produit.id); // indice du produit
		if (catalog[index].name.toLowerCase().indexOf(filterText.toLowerCase()) > -1) {
			produit.style.display = ""; // on affiche le produit si la condition est evalué à (true).
		}

		else {
			produit.style.display = "none"; // on le masque sinon.
		}
	}
}


// utilisation de l'API webstorage (localStorage) (optionnel)

/**
 * @todo creation du boutton pour le stockage local.
 */
function buttonStorage() {

	 // creation du boutton stockage(div, id,..) et l'ajouter a #Panier
	const panier = document.getElementById("panier");
	const divStock = document.createElement("div");
	divStock.id = "stockage";

	const buttonStorage = document.createElement("button")
	buttonStorage.id = "storage";

	buttonStorage.textContent = "stocker le panier";
	buttonStorage.disabled = true; // desactivation du boutton stockage.

	divStock.appendChild(buttonStorage);
	panier.appendChild(divStock);
	buttonStorage.addEventListener("click", saveBasket); // mise en place de l'evenement pour stocker les articles.
}


/**
 * @todo stockage des articles du panier dans localStorage.
 */
function saveBasket() {
	const montantPanier = document.getElementById("montant").textContent;
	localStorage.setItem("articles", JSON.stringify(cart));
	localStorage.setItem("montant", montantPanier);
}



/**
 * @todo remise en place des articles du panier au chargement de la page.
 */
function getBasket() {
	if (localStorage.length !== 0) {

		const articles = JSON.parse(localStorage.getItem("articles")); // recuperation depuis localStorage
		const panier = document.getElementById("panier");
		const achats = panier.querySelector(".achats");

		for (const article of articles) {
			const index = parseInt(article.id);
			const div = createDivAchat(index, article.qty);
			achats.appendChild(div);
			cart.push(catalog[index]);
			cart[(cart.indexOf(catalog[index]))].qty = article.qty;
			const suppression = document.getElementById(index + "-remove");
			suppression.addEventListener("click", supprimeProduct);
		}
		const montantPanier = document.getElementById("montant");
		montantPanier.textContent = parseInt(localStorage.getItem("montant")); 
		total = parseInt(montantPanier.textContent);
	}
}


/**function inputP(index) {
	const input = document.createElement("input");
	input.id = index + "-qteP";
	input.classList.add("inputP")
	input.type = "number";
	input.step = "1";
	input.value = "0";
	input.min = "0";
	input.max = MAX_QTY.toString();
	
	return input;

}*/














/**const filterText = document.getElementById("filter").value;
for (const article of catalog){
	const produit = catalog.indexOf(article) + '-product';
	if (article.name.includes(filterText)){
		document.getElementById(produit).style.display = "inline-block";
	}

	else {
		document.getElementById(produit).style.display = "none"
	}
}
}**/




/**const elements = Array.from(document.querySelectorAll(".produit"));
	console.log("element", elements);
	elements.forEach(element => {
		element.style.display = "";
	});

	elements.filter(element => !element.innerText.toLowerCase().includes(filterText.toLowerCase()))
	.forEach(element => element.style.display = "none");*/



